﻿using System;
using System.Drawing;
using System.Linq;

namespace KmeansAlgorithm
{
    internal class KmeansAlgorithm
    {
        private readonly Point[] _points;

        private readonly PointsArea[] _pointsAreas;

        public bool IsChanged = true;

        public KmeansAlgorithm(int coreNumber, int pointNumber)
        {
            _points = new Point[pointNumber];
            _pointsAreas = new PointsArea[coreNumber];
            InitializePointsAreas();
        }

        private void InitializePointsAreas()
        {
            for (var i = 0; i < _pointsAreas.Length; i++)
            {
                _pointsAreas[i] = new PointsArea();
            }
        }

        private void CreateRandomPoints(int width, int height)
        {
            var randomCoordinate = new Random();
            
            for (var i = 0; i < _points.Length; i++)
            {
                _points[i] = new Point(randomCoordinate.Next(width), randomCoordinate.Next(height));
            }
        }

        private void ChooseCorePoints()
        {
            var randomCore = new Random();

            foreach (var pointArea in _pointsAreas)
            {
                pointArea.Core = _points[randomCore.Next(_points.Length)];
            }

            DefineAreas();
        }

        private double CountDistance(Point core, Point point)
        {
            return Math.Sqrt(Math.Pow(core.X - point.X, 2) + Math.Pow(core.Y - point.Y, 2));
        }

        private int FindMinimalDistance(Point point)
        {
            var minimalDistanceIndex = 0;

            for (var i = 0; i < _pointsAreas.Length; i++)
            {
                if (CountDistance(_pointsAreas[i].Core, point) <
                    CountDistance(_pointsAreas[minimalDistanceIndex].Core, point))
                    minimalDistanceIndex = i;
            }

            return minimalDistanceIndex;
        }

        private void DefineAreas()
        {
            foreach (var pointArea in _pointsAreas)
            {
                pointArea.Points.Clear();
            }

            foreach (var point in _points)
            {
                _pointsAreas[FindMinimalDistance(point)].Points.Add(point);
            }
        }

        public PointsArea[] StartAlgorithm(int width, int height)
        {
            CreateRandomPoints(width, height);
            ChooseCorePoints();

            return _pointsAreas;
        }

        public PointsArea[] ImprovePointsAreas()
        {
            
            ChangeCorePoints();
            DefineAreas();
            return _pointsAreas;
        }

        private void ChangeCorePoints()
        {
            IsChanged = false;

            foreach(var pointsArea in _pointsAreas)
            {
                var averagePoint = new Point(Convert.ToInt32(pointsArea.Points.Average(x => x.X)), Convert.ToInt32(pointsArea.Points.Average(y => y.Y)));
                var minimalCoreDistance = CountDistance(pointsArea.Core, averagePoint);

                foreach (var checkPoint in pointsArea.Points)
                {
                    var distance = CountDistance(averagePoint, checkPoint);

                    if (distance < minimalCoreDistance)
                    {
                        minimalCoreDistance = distance;
                        pointsArea.Core = checkPoint;
                        IsChanged = true;
                    }
                }

            }
        }
    }
}