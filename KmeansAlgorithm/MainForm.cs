﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace KmeansAlgorithm
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void calculateButton_Click(object sender, EventArgs e)
        {
            int kCount, nCount;
            if (int.TryParse(kCountTextBox.Text, out kCount) && int.TryParse(nCountTextBox.Text, out nCount) &&
                kCount >= 2 && kCount <= 20 && nCount >= 1000 && nCount <= 100000)
            {
                var algorithm = new AlgorithmVisualization(kCount, nCount);

                using (var graphics = algorithmBox.CreateGraphics())
                {
                    algorithm.Visualizate(graphics, algorithmBox.Width, algorithmBox.Height);
                }

            }
            else
            {
                MessageBox.Show("Проверьте введенные данные!");
            }

        }
    }
}
