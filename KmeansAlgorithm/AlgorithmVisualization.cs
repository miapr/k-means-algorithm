﻿using System;
using System.Drawing;

namespace KmeansAlgorithm
{
    public class AlgorithmVisualization
    {
        private readonly KmeansAlgorithm _kmeansAlgorithm;
        private const int PointWidth = 5;
        private readonly Color[] _spotColors = new Color[20];

        public AlgorithmVisualization(int coreNumber, int pointNubmer)
        {
            _kmeansAlgorithm = new KmeansAlgorithm(coreNumber, pointNubmer);
            GenerateRandomColors();
        }

        private void GenerateRandomColors()
        {
            var random = new Random();
            for (var i = 0; i < _spotColors.Length; i++)
            {
                _spotColors[i] = Color.FromArgb(random.Next(0, 255), random.Next(0, 255), random.Next(0, 255));
            }

        }

        public void Visualizate(Graphics graphics, int width, int height)
        {
            graphics.Clear(Color.White);
            var pointAreas = _kmeansAlgorithm.StartAlgorithm(width, height);

            while (_kmeansAlgorithm.IsChanged)
            {
                _kmeansAlgorithm.ImprovePointsAreas();
            }

            for (var i = 0; i < pointAreas.Length; i++)
            {
                DrawPoints(graphics, pointAreas[i], _spotColors[i]);
            }

        }

        private void DrawPoints(Graphics graphics, PointsArea pointArea, Color color)
        {
            using (var brush = new SolidBrush(color))
            {
                foreach (var point in pointArea.Points)
                {
                    graphics.FillEllipse(brush, point.X, point.Y, PointWidth, PointWidth);
                }

                brush.Color = Color.Black;
                graphics.FillEllipse(brush, pointArea.Core.X, pointArea.Core.Y, PointWidth, PointWidth);
            }
        }
    }
}