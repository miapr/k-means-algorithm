﻿using System.Collections.Generic;
using System.Drawing;

namespace KmeansAlgorithm
{
    internal class PointsArea
    {
        public List<Point> Points { get; set; } = new List<Point>();
        public Point Core { get; set; }
    }
}