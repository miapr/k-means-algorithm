﻿namespace KmeansAlgorithm
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.calculateButton = new System.Windows.Forms.Button();
            this.algorithmBox = new System.Windows.Forms.PictureBox();
            this.kCountTextBox = new System.Windows.Forms.TextBox();
            this.nCountTextBox = new System.Windows.Forms.TextBox();
            this.InfoClassLabel = new System.Windows.Forms.Label();
            this.infoLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.algorithmBox)).BeginInit();
            this.SuspendLayout();
            // 
            // calculateButton
            // 
            this.calculateButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.calculateButton.Location = new System.Drawing.Point(669, 9);
            this.calculateButton.Name = "calculateButton";
            this.calculateButton.Size = new System.Drawing.Size(124, 23);
            this.calculateButton.TabIndex = 0;
            this.calculateButton.Text = "Посчитать";
            this.calculateButton.UseVisualStyleBackColor = true;
            this.calculateButton.Click += new System.EventHandler(this.calculateButton_Click);
            // 
            // algorithmBox
            // 
            this.algorithmBox.BackColor = System.Drawing.Color.White;
            this.algorithmBox.Location = new System.Drawing.Point(28, 47);
            this.algorithmBox.Name = "algorithmBox";
            this.algorithmBox.Size = new System.Drawing.Size(765, 481);
            this.algorithmBox.TabIndex = 1;
            this.algorithmBox.TabStop = false;
            // 
            // kCountTextBox
            // 
            this.kCountTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.kCountTextBox.Location = new System.Drawing.Point(245, 9);
            this.kCountTextBox.Name = "kCountTextBox";
            this.kCountTextBox.Size = new System.Drawing.Size(77, 22);
            this.kCountTextBox.TabIndex = 2;
            this.kCountTextBox.Text = "5";
            // 
            // nCountTextBox
            // 
            this.nCountTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.nCountTextBox.Location = new System.Drawing.Point(572, 9);
            this.nCountTextBox.Name = "nCountTextBox";
            this.nCountTextBox.Size = new System.Drawing.Size(77, 22);
            this.nCountTextBox.TabIndex = 3;
            this.nCountTextBox.Text = "10000";
            // 
            // InfoClassLabel
            // 
            this.InfoClassLabel.AutoSize = true;
            this.InfoClassLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.InfoClassLabel.Location = new System.Drawing.Point(25, 12);
            this.InfoClassLabel.Name = "InfoClassLabel";
            this.InfoClassLabel.Size = new System.Drawing.Size(214, 16);
            this.InfoClassLabel.TabIndex = 4;
            this.InfoClassLabel.Text = "Количество классов (от 2 до 20)";
            // 
            // infoLabel
            // 
            this.infoLabel.AutoSize = true;
            this.infoLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.infoLabel.Location = new System.Drawing.Point(328, 12);
            this.infoLabel.Name = "infoLabel";
            this.infoLabel.Size = new System.Drawing.Size(238, 16);
            this.infoLabel.TabIndex = 5;
            this.infoLabel.Text = "Количество образов (от 1к до 100к)";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(830, 550);
            this.Controls.Add(this.infoLabel);
            this.Controls.Add(this.InfoClassLabel);
            this.Controls.Add(this.nCountTextBox);
            this.Controls.Add(this.kCountTextBox);
            this.Controls.Add(this.algorithmBox);
            this.Controls.Add(this.calculateButton);
            this.Name = "MainForm";
            this.Text = "Алгоритм K-средних";
            ((System.ComponentModel.ISupportInitialize)(this.algorithmBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button calculateButton;
        private System.Windows.Forms.PictureBox algorithmBox;
        private System.Windows.Forms.TextBox kCountTextBox;
        private System.Windows.Forms.TextBox nCountTextBox;
        private System.Windows.Forms.Label InfoClassLabel;
        private System.Windows.Forms.Label infoLabel;
    }
}

